<?php

########################################################################
# Extension Manager/Repository config file for ext "t3_carousel".
#
# Auto generated 30-10-2013 14:59
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Slider (bootstrap)',
	'description' => '',
	'category' => 'plugin',
	'author' => 'Gunther Schoebinger',
	'author_email' => 'g.schoebinger@t3user.de',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:14:{s:9:"ChangeLog";s:4:"490b";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"9855";s:14:"ext_tables.php";s:4:"6435";s:13:"locallang.xml";s:4:"113f";s:16:"locallang_db.xml";s:4:"e480";s:19:"doc/wizard_form.dat";s:4:"fd56";s:20:"doc/wizard_form.html";s:4:"5d77";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:31:"pi1/class.tx_t3carousel_pi1.php";s:4:"9faa";s:39:"pi1/class.tx_t3carousel_pi1_wizicon.php";s:4:"3c4d";s:13:"pi1/clear.gif";s:4:"cc11";s:17:"pi1/locallang.xml";s:4:"00c1";}',
);

?>