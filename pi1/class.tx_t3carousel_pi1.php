<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2013 Gunther Schoebinger <g.schoebinger@t3user.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

$compatibilityFuncs = Tx_Formhandler_CompatibilityFuncs::getInstance();
if($compatibilityFuncs->convertVersionNumberToInteger(TYPO3_version) < 6002000) {
	require_once(PATH_tslib . 'class.tslib_pibase.php');
}

/**
 * Plugin 'Slider' for the 't3_carousel' extension.
 *
 * @author	Gunther Schoebinger <g.schoebinger@t3user.de>
 * @package	TYPO3
 * @subpackage	tx_t3carousel
 */
class tx_t3carousel_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_t3carousel_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_t3carousel_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 't3_carousel';	// The extension key.
	var $pi_checkCHash = false;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		
		// flexform
		$this->pi_initPIflexForm();
		$this->myflexform['general'] = $this->cObj->data['pi_flexform']['data']['sDEF']['lDEF'];
		$this->myflexform['options'] = $this->cObj->data['pi_flexform']['data']['tDEF']['lDEF'];
	
		// css + js
		$GLOBALS['TSFE']->additionalHeaderData[$this->extKey] .= '<link rel="stylesheet" href="'.t3lib_extMgm::siteRelPath($this->extKey).'res/carousel.css">';
		
		// autostart
		if ($this->myflexform['options']['startDelay']['vDEF']) {
			$GLOBALS['TSFE']->additionalFooterData[$this->extKey] .= '
			<script type=\'text/javascript\'>
			    $(document).ready(function() {
			         $(\'.carousel\').carousel({
			             interval: '.$this->myflexform['options']['startDelay']['vDEF'].'
			         })
			    });    
			</script>
			';
		}
		
		$myImages = $this->myflexform['general']['titleContainer']['el'];
		
		$content = '<div id="carousel-example-generic" class="carousel slide">';
  		
		// shuffle
		if ($this->myflexform['options']['enablerandom']['vDEF']) {	
			$numbers = range(1, count($myImages));
			shuffle($numbers);
			foreach ($numbers as $k => $v) {
				$myNewImages[$k] = $myImages[$v];
			}
			$myImages = $myNewImages;
		}
		
  		// indicators
		if ($this->myflexform['options']['enableindicators']['vDEF']) {	
			$content .= '<!-- Indicators -->';
			$content .= '<ol class="carousel-indicators">';
	  		$i = 0;
			foreach ($myImages as $k => $v) {
					
				$i++;
				if ($i==1) {
					$addClass = 'active';
				} else {
					$addClass = '';
				}
				 
				$content .= '<li data-target="#carousel-example-generic" data-slide-to="'.($i-1).'" class="'.$addClass.'"></li>';
			}
			$content .= '</ol>';
		}
		
		$content .= '<!-- Wrapper for slides -->';
		$content .= '<div class="carousel-inner">';
			 
		$i = 0;
		foreach ($myImages as $k => $v) {
			
			$i++;
			if ($i==1) {
				$addClass = 'active';
			} else {
				$addClass = '';
			}
				 
			$content .= '<div class="item '.$addClass.'">';
			$content .= '<img src="uploads/tx_t3carousel/'.$v['titleWrap']['el']['image']['vDEF'].'" alt="'.$v['titleWrap']['el']['imgDescription']['vDEF'].'" title="'.$v['titleWrap']['el']['imgTitle']['vDEF'].'" />';
			
			if ($this->myflexform['options']['enabletitle']['vDEF'] && (strlen($v['titleWrap']['el']['imgDescription']['vDEF'])>0 || strlen($v['titleWrap']['el']['imgTitle']['vDEF'])>0)) {
				$content .= '<div class="carousel-caption">';
				if (strlen($v['titleWrap']['el']['imgTitle']['vDEF'])>0) {
					$content .= '<h3>'.$v['titleWrap']['el']['imgTitle']['vDEF'].'</h3>';
				}
				if (strlen($v['titleWrap']['el']['imgDescription']['vDEF'])>0) {
					$content .= '<p>'.nl2br($v['titleWrap']['el']['imgDescription']['vDEF']).'</p>';
				}
				$content .= '</div>';
			}
			
			
			$content .= '</div>';
		}
		$content .= '</div>';
		
  		
  		// controls
		if ($this->myflexform['options']['enabledecontrol']['vDEF']) {	
			$content .= '<!-- Controls -->';
			$content .= '<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">';
			$content .= '<span class="icon-prev"></span>';
			$content .= '</a>';
			$content .= '<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">';
			$content .= '<span class="icon-next"></span>';
			$content .= '</a>';
		}
		
		$content .= '</div>';
		
		
	
		return $content;
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/t3_carousel/pi1/class.tx_t3carousel_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/t3_carousel/pi1/class.tx_t3carousel_pi1.php']);
}

?>